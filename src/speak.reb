Rebol [
    title: "Send a message to chat"
    file: %speak.reb
    date: 24-Feb-2020
    notes: {function to post a chat message to a room}
]

speak: func [ chat-account [object!] content [text!]
    <local> website obj response
][
    if not integer? chat-account/room [return "No room specified in chat-account object"]

    if get in chat-account 'site [
        website: chat-account/site
        obj: make chat-account compose [content: (content) replyto: _]
        response: write to url! unspaced [website "/rooms/" chat-account/room ] reduce ['POST (mold obj)]
        probe to text! response
    
    ] else [
        print "No chat-server specified yet.  Maybe you're not logged in?"
    ]
]
