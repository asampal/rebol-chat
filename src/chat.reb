Rebol [
    title: "Rebol client chat"
    file: %chat.reb
    date: 24-Feb-2019
    notes: {collect all the related chat functions}
]

if system/version = 2.102.0.16.2 [
    Print "wasm based chat not currently working"
    halt
]

number: charset [#"0" - #"9"]
numbers: [some number]


; register
do https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/register.reb
; login webserver carl@rebol.com
do https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/login.reb
; speak
do https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/speak.reb
; add icon
do https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/add-icon.reb

print {^/^/Make sure that you have gpg installed and available to the rebol console, and
make sure that you have a gitlab/gihub raw page such as 

https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/users/graham.md already created

setup GPG: https://gitlab.com/Zhaoshirong/rebol-chat/-/blob/master/README.md
setup registration page: https://gitlab.com/Zhaoshirong/rebol-chat/-/blob/master/instructions.md


Usage:

chat-server is currently: http://35.224.174.22

; to register with the chat-server
register chat-server

; to login
o: login chat-server 'username [word!] youremail@whereever.com [email!]

This sets a chat object in the block `accounts`.  
  o/room: 1 ; set chat to room 1
  speak o "This is my message" ; send a message to chat room 1
}

