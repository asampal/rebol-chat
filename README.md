# rebol-chat

Prototyping a Rebol chat system using wasm and httpd

## Notes

## Setup GPG

Generate your keys

for quick generation

```
gpg --gen-key
```

or, for more options

```
gpg --full-generate-key
```


### Exporting Your Public Key

```
gpg --export --armor --export your-email@some.com > mykey.asc
```

### Importing the Chat Server's public key

```
wget http://35.224.174.22/rebolchat.asc
```

and then import

```
gpg --import rebolchat.asc
```

### Command Line Decryption

This method means the passphrase does not need to be entered in the popup

```
gpg --pinentry-mode=loopback --passphrase passphrase -d -o challenge.txt challenge.gpg
```

### Command Line Encryption

Where the PGP public key for recipient-email is in my keystore

```
gpg --trust-model always --recipient recipient-email --armour --encrypt file-to-encrypt
```